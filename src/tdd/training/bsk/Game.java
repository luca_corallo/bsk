package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	
	private List<Frame> listFrame;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		listFrame = new ArrayList<>();
		firstBonusThrow = 0;
		secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(areLastAndPenultimateFramesStrike())
			setDoubleStrikeBonus(frame);
		if(isLastFrameAStrike())
			setStrikeBonusOfLastFrame(frame);
		else if(isLastFrameASpare())
			setSpareBonusOfLastFrame(frame);
		listFrame.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return listFrame.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
		updatetBonusToLastFrame(firstBonusThrow);
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
		updatetBonusToLastFrame(secondBonusThrow);
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int gameScore = 0;
		for(Frame frame : listFrame)
			gameScore += frame.getScore();
		
		if(gameScore == 290) gameScore += 10;
		return gameScore;
	}
	
	private boolean isLastFrameASpare() {
		return (listFrame.size() > 0 && listFrame.get(listFrame.size() - 1).isSpare());
	}
	
	private boolean isLastFrameAStrike() {
		return (listFrame.size() > 0 && listFrame.get(listFrame.size() - 1).isStrike());
	}
	
	private boolean areLastAndPenultimateFramesStrike() {
		return (listFrame.size() > 1 &&
				listFrame.get(listFrame.size() - 1).isStrike() &&
				listFrame.get(listFrame.size() - 2).isStrike());
	}
	
	private void updateBonusOfLastFrame(int offset, int bonus) {
		Frame lastFrame = listFrame.get(listFrame.size() - offset);
		lastFrame.setBonus(bonus);
		listFrame.set(listFrame.size() - offset, lastFrame);
	}
	
	private void setSpareBonusOfLastFrame(Frame newFrame) {
		updateBonusOfLastFrame(1, newFrame.getFirstThrow());
	}
	
	private void setStrikeBonusOfLastFrame(Frame newFrame) {
		updateBonusOfLastFrame(1, newFrame.getFirstThrow() + newFrame.getSecondThrow());
	}
	
	private void setDoubleStrikeBonus(Frame newFrame) {
		updateBonusOfLastFrame(2, 10 + newFrame.getFirstThrow());
	}
	
	private void updatetBonusToLastFrame(int bonus) {
		Frame lastFrame = listFrame.get(listFrame.size() - 1);
		lastFrame.setBonus(lastFrame.getBonus() + bonus);
		listFrame.set(listFrame.size() - 1, lastFrame);
	}

}
