package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void gameAt2ShouldHave7And2AsThrows() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 5);
		Frame frame2 = new Frame(3, 6);
		Frame frame3 = new Frame(7, 2);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		
		//Assert
		assertEquals(frame3, game.getFrameAt(2));		
	}
	
	@Test
	public void calculateScoreOfThisGameShouldBe81() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 5);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(2, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		
		//Assert
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void spareScoreShouldBeWithABonus() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(5, 5);
		Frame frame2 = new Frame(3, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		
		//Assert
		assertEquals(13, frame1.getScore());
	}
	
	@Test
	public void scoreWithSpareShouldBe88() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 9);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(2, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		
		//Assert
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void strikeScoreShouldBeWithABonus() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(3, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		
		//Assert
		assertEquals(19, frame1.getScore());
	}
	
	@Test
	public void scoreWithStrikeShouldBe94() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(2, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		
		//Assert
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void scoreWithStrikeFollowedBySpareShouldBe103() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(4, 6);
		Frame frame10 = new Frame(2, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		
		//Assert
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void scoreWithStrikeFollowedByStrikeShouldBe112() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(10, 0);
		Frame frame10 = new Frame(2, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		
		//Assert
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void scoreWithSpareFollowedBySpareShouldBe98() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(8, 2);
		Frame frame2 = new Frame(5, 5);
		Frame frame10 = new Frame(2, 6);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		
		//Assert
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void spareShouldHaveFirstBonusThrow() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 5);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(2, 8);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		
		//Assert
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void scoreWithLastFrameAsSpareShouldBe90() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 5);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(2, 8);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		
		//Assert
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void scoreWithLastFrameAsStrikeShouldBe92() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 5);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(10, 0);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		//Assert
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void strikeShouldHaveSecondBonusThrowToo() throws Exception {
		//Arrange
		Game game;
		Frame frame1 = new Frame(1, 5);
		Frame frame2 = new Frame(3, 6);
		Frame frame10 = new Frame(10, 0);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		setFrom3To9Frames(game);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		//Assert
		assertEquals(2, game.getSecondBonusThrow());
	}
	

	@Test
	public void shouldBeAPerfectGame() throws Exception {
		//Arrange
		Game game;

		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(10, 0);
		Frame frame3 = new Frame(10, 0);
		Frame frame4 = new Frame(10, 0);
		Frame frame5 = new Frame(10, 0);
		Frame frame6 = new Frame(10, 0);
		Frame frame7 = new Frame(10, 0);
		Frame frame8 = new Frame(10, 0);
		Frame frame9 = new Frame(10, 0);
		Frame frame10 = new Frame(10, 0);
		
		//Act
		game = new Game();
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		//Assert
		assertEquals(300, game.calculateScore());
	}
	
	
	private void setFrom3To9Frames(Game game) throws Exception {
		Frame frame3 = new Frame(7, 2);
		Frame frame4 = new Frame(3, 6);
		Frame frame5 = new Frame(4, 4);
		Frame frame6 = new Frame(5, 3);
		Frame frame7 = new Frame(3, 3);
		Frame frame8 = new Frame(4, 5);
		Frame frame9 = new Frame(8, 1);
		
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
	}
}
