package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test(expected = BowlingException.class)
	public void negativePinsInFirstThrowShouldBeInvalid() throws Exception {
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(-1, 2);
	}
	
	@Test(expected = BowlingException.class)
	public void negativePinsInSecondThrowShouldBeInvalid() throws Exception {
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(1, -2);
	}
	
	@Test(expected = BowlingException.class)
	public void morePinsThan10ShouldBeInvalid() throws Exception {
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(10, 1);
	}
	
	@Test
	public void frameShouldReturn2And4AsThrows() throws Exception{
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(2, 4);
		
		//Assert
		assertEquals(2, frame.getFirstThrow());
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void scoreOf2And6ShouldBe8() throws Exception {
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(2, 6);
		
		//Assert
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void shouldBeASpare() throws Exception {
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(1, 9);
		
		//Assert
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void shouldBeAStrike() throws Exception {
		//Arrange
		Frame frame;
		
		//Act
		frame = new Frame(10, 0);
		
		assertTrue(frame.isStrike());
	}

}
